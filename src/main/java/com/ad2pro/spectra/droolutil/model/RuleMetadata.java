package com.ad2pro.spectra.droolutil.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
public class RuleMetadata {
    private String path;
    private List<Rule> rules;
}
