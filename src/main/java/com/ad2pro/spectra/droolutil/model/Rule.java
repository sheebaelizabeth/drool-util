package com.ad2pro.spectra.droolutil.model;


import com.ad2pro.spectra.droolutil.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Rule {

    private final String name;
    private String group;
    private List<Condition> conditions = new ArrayList<Condition>();
    private String action;

    public Rule(String name)
    {
        this.name = name;
    }

    public Rule(String name, String group, List<Condition> conditions, String action) {
        this.name = name;
        this.group = group;
        this.conditions = conditions;
        this.action = action;
    }

    public enum Attribute {
        RULE_NAME("name"),
        GROUP("group"),
        CONDITIONAL("conditional"),
        ACTION("action");

        private final String name;

        private Attribute(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    }

    @Override
    public String toString() {
        StringBuilder me = new StringBuilder("[" + this.getClass().getName());
        me.append(" | name = ");
        me.append(name);
        me.append(" | object = ");
        me.append(group);
        me.append(" | conditions = ");
        me.append(((conditions == null) ? "null" : conditions.size()));
        me.append(" | action = ");
        me.append(action);
        me.append("]");

        return me.toString();
    }


    public String conditionAsDRL() throws IllegalStateException, IllegalArgumentException {
        if ((conditions == null) || (conditions.isEmpty())) {
            throw new IllegalStateException("You must declare at least one condition to be evaluated.");
        }

        StringBuilder drl = new StringBuilder();
        //For each condition of this rule, we create its textual representation
        for (int i = 0; i < conditions.size(); i++) {
            Condition condition = conditions.get(i);
            drl.append("(");
            drl.append(condition.buildExpression());
            drl.append(")");
            if ((i + 1) < conditions.size())
            {
                drl.append(" && ");
            }
        }

        return drl.toString();
    }

    public Map<String, Object> asMap() throws IllegalStateException {
        if ((name == null)  || (action == null))
            throw new IllegalArgumentException("The rule has no name,or action to be accomplished.");
        if(group == null)
            group = Constants.DEFAULT_GROUP;
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(Rule.Attribute.RULE_NAME.toString(), name);
        attributes.put(Attribute.GROUP.toString(), group);
        attributes.put(Rule.Attribute.CONDITIONAL.toString(), conditionAsDRL());
        attributes.put(Rule.Attribute.ACTION.toString(), action);

        return attributes;
    }

    public Condition addCondition(String property, Condition.Operator operator, Object value) {
        Condition condition = new Condition(property, operator, value);
        conditions.add(condition);

        return condition;
    }

    public String getName()
    {
        return name;
    }

    public String getGroup()
    {
        return group;
    }

    public List<Condition> getConditions()
    {
        return conditions;
    }

    public Condition getCondition() {
        if ((conditions == null) || (conditions.isEmpty())) {
            return null;
        }
        else {
            return conditions.get(0);
        }
    }

    public void setConditions(List<Condition> conditions)
    {
        this.conditions = conditions;
    }

    public void setCondition(Condition condition) {
        conditions = new ArrayList<Condition>();
        conditions.add(condition);
    }

    public String getAction()
    {
        return action;
    }

    public void setDataObject(String dataObject)
    {
        this.group = dataObject;
    }

    public void setAction(String action)
    {
        this.action = action;
    }
}
