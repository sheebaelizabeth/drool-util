package com.ad2pro.spectra.droolutil.engine;

import com.ad2pro.spectra.droolutil.exceptions.NoRuleFoundException;
import com.ad2pro.spectra.droolutil.model.Rule;
import com.ad2pro.spectra.droolutil.model.RuleMetadata;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.Message;
import org.kie.api.event.KieRuntimeEventManager;
import org.kie.api.io.KieResources;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;

import java.util.*;

@Slf4j
public abstract class AbstractRuleEngine {

    protected KieContainer kieContainer;
    protected KieServices kieServices;
    protected KieResources kieResources;
    protected KieFileSystem kieFileSystem;
    protected KieRepository kieRepository;
    protected KieRuntimeEventManager runtime;
    protected Set<String> resourcePaths = new HashSet<>();

    private Map<String, List<Rule>> ruleContents = new HashMap<>();

    public AbstractRuleEngine() {
        this.kieServices = KieServices.Factory.get();
        this.kieResources = kieServices.getResources();
        this.kieFileSystem = kieServices.newKieFileSystem();
        this.kieRepository = kieServices.getRepository();
    }

    public List<Rule> addRules(RuleMetadata ruleMetadata, String drl) {
        String rulefile = ruleMetadata.getPath();
        System.out.println("drl:\n" + drl);
        log.info("DRL Content {}", drl);
        Resource resource = null;
        resource = kieResources.newByteArrayResource(drl.getBytes());

        if(resource == null) {
            return new ArrayList<>();
        }

        String resourcepath = "src/main/resources/drools/templates/"+rulefile;

        if(resourcePaths.contains(resourcepath)) {
            log.warn("removing existing rule from the resource path: {}", resourcepath);
            removeRule(rulefile);
        }

        kieFileSystem.write(resourcepath, resource);
        resourcePaths.add(resourcepath);
        ruleContents.put(rulefile, ruleMetadata.getRules());
        log.info("Rule added successfully");
        buildKnowledgeBase();
        return ruleMetadata.getRules();
    }

    public void removeRule(String rulename) {
        log.info("Removing rule {}", rulename);
        ruleContents.remove(rulename);
        resourcePaths.remove(rulename);
        kieFileSystem.delete(rulename);
    }

    public Map<String, List<Rule>> getRuleContents() {
        return ruleContents;
    }

    public Optional<List<Rule>> getRuleByPath(String path) throws NoRuleFoundException {
        List<Rule> rules = ruleContents.get(path);
        if(null == rules)
            throw new NoRuleFoundException("No rule file found for path " +path);
        return Optional.of(rules);
    }

    public void buildKnowledgeBase() {
        log.info("Building knowledge base");
        KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
        kb.buildAll();

        if (kb.getResults().hasMessages(Message.Level.ERROR)) {
            throw new RuntimeException("Build Errors:\n" + kb.getResults().toString());
        }
        kieContainer = kieServices.newKieContainer(kieRepository.getDefaultReleaseId());
        log.info("Knowledge Base build done");
    }

    public KieContainer getContainer() {
        return kieContainer;
    }

}
