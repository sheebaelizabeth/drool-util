package com.ad2pro.spectra.droolutil.engine;

import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.event.KieRuntimeEventManager;
import org.kie.api.event.rule.*;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RuleEngine extends AbstractRuleEngine {

    public RuleEngine() {
    }

    public KieSession createSession() {
        KieBaseConfiguration kieBaseConfiguration = kieServices.newKieBaseConfiguration();
        KieBase kieBase = kieContainer.newKieBase(kieBaseConfiguration);
        KieSession kieSession = kieBase.newKieSession();
        kieSession.addEventListener(new AgendaEventListener() {

            @Override
            public void matchCreated(MatchCreatedEvent event) {
                System.out.println("The rule "
                        + event.getMatch().getRule().getName()
                        + " can be fired in agenda");
            }

            @Override
            public void matchCancelled(MatchCancelledEvent event) {
                System.out.println("The rule "
                        + event.getMatch().getRule().getName()
                        + " cannot b in agenda");
            }

            @Override
            public void beforeMatchFired(BeforeMatchFiredEvent event) {
                System.out.println("The rule "
                        + event.getMatch().getRule().getName()
                        + " will be fired");
            }

            @Override
            public void afterMatchFired(AfterMatchFiredEvent event) {
                System.out.println("The rule "
                        + event.getMatch().getRule().getName()
                        + " has be fired");
            }

            @Override
            public void agendaGroupPopped(AgendaGroupPoppedEvent event) {
            }

            @Override
            public void agendaGroupPushed(AgendaGroupPushedEvent event) {
            }

            @Override
            public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
            }

            @Override
            public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
            }

            @Override
            public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
            }

            @Override
            public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
            }
        });
        return kieSession;
    }

//    public void setGlobal(String identifier, Object value) {
//        kieSession.setGlobal(identifier, value);
//    }

    public int fireAllRules(KieSession kieSession) {
        return kieSession.fireAllRules();
    }

//    public <T> T getGlobal(String identifier, Class<T> clazz) {
//        return (T)kieSession.getGlobal(identifier);
//    }


    public void dispose(KieSession kieSession) {
        if (kieSession != null) {
            kieSession.dispose();
        }
    }

    public FactHandle insert(KieSession kieSession, Object fact) {
        return kieSession.insert(fact);
    }
}
