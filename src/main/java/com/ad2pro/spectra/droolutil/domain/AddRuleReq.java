package com.ad2pro.spectra.droolutil.domain;

import com.ad2pro.spectra.droolutil.model.Rule;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AddRuleReq implements Serializable {

    private static final long serialVersionUID = 4492274033897337423L;

    private Long corporateId;
    private List<Rule> rules;

}
