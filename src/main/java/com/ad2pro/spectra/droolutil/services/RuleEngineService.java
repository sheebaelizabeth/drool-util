package com.ad2pro.spectra.droolutil.services;


import com.ad2pro.spectra.droolutil.engine.RuleEngine;
import com.ad2pro.spectra.droolutil.exceptions.NoRuleFoundException;
import com.ad2pro.spectra.droolutil.model.Rule;
import com.ad2pro.spectra.droolutil.model.RuleMetadata;
import com.ad2pro.spectra.droolutil.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class RuleEngineService {

    @Autowired
    private RuleEngine ruleEngine;

    public Optional<List<Rule>> addRules(RuleMetadata ruleMetadata) {
        if(null == ruleMetadata || null == ruleMetadata.getPath() || null == ruleMetadata.getRules())
            throw new IllegalArgumentException("Rules or path field missing");
        String ruleContent = getRuleContent(ruleMetadata.getRules());
        List<Rule> rules = ruleEngine.addRules(ruleMetadata, ruleContent);
        return Optional.of(rules);
    }

    public Optional<List<Rule>> getRuleByPath(String path) throws NoRuleFoundException {
        return ruleEngine.getRuleByPath(path);
    }

    public Map insertFact(String groupName, Map fact) {
        KieSession kieSession = ruleEngine.createSession();
        kieSession.getAgenda().getAgendaGroup(groupName).setFocus();
        log.info("Focus set to group {}", groupName);
        ruleEngine.insert(kieSession,fact);
        ruleEngine.fireAllRules(kieSession);
        return fact;
    }

    public Map insertFact(Map fact) {
        KieSession kieSession = ruleEngine.createSession();
        ruleEngine.insert(kieSession,fact);
        ruleEngine.fireAllRules(kieSession);
        return fact;
    }

    private String getRuleContent(List<Rule> rules) {
        log.info("Converting list of rules to DRL");
        List<Map<String, Object>> rulesAsParameters = new ArrayList<Map<String, Object>>(rules.size());
        for (Rule rule : rules) {
            rulesAsParameters.add(rule.asMap());
        }
        ObjectDataCompiler compiler = new ObjectDataCompiler();
        String drl = compiler.compile(rulesAsParameters, Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.TEMPLATE_PATH));
        return drl;
    }

}
