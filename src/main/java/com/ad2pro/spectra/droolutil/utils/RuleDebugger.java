package com.ad2pro.spectra.droolutil.utils;

import lombok.extern.slf4j.Slf4j;
import org.drools.core.spi.KnowledgeHelper;

@Slf4j
public class RuleDebugger {

    public static void debug(final KnowledgeHelper helper) {
        System.out.println("Triggered rule: " + helper.getRule().getName());
        if (helper.getMatch() != null && helper.getMatch().getObjects() != null) {
            for (Object object : helper.getMatch().getObjects()) {
                System.out.println("Data object: " + object);
            }
        }
    }
}
