package com.ad2pro.spectra.droolutil.utils;


public class Constants {

    public static final String TEMPLATE_PATH = "drools/templates/RuleTemplate.drl";

    public static final String DEFAULT_GROUP = "MAIN";
}
