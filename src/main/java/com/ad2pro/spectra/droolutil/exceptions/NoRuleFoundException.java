package com.ad2pro.spectra.droolutil.exceptions;


public class NoRuleFoundException extends Exception{

    public NoRuleFoundException(String message) {
        super(message);
    }
}
