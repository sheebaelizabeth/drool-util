package com.ad2pro.spectra.droolutil.controllers;


import com.ad2pro.spectra.droolutil.domain.AddRuleReq;
import com.ad2pro.spectra.droolutil.exceptions.NoRuleFoundException;
import com.ad2pro.spectra.droolutil.model.Rule;
import com.ad2pro.spectra.droolutil.model.RuleMetadata;
import com.ad2pro.spectra.droolutil.services.RuleEngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class RuleController {

    @Autowired
    private RuleEngineService ruleEngineService;

    @RequestMapping(method = RequestMethod.POST, value = "/drool/addrule")
    public Optional<List<Rule>> setRule(@RequestBody AddRuleReq addRuleReq) {
        RuleMetadata ruleMetadata = new RuleMetadata();
        ruleMetadata.setPath(addRuleReq.getCorporateId() + ".drl");
        ruleMetadata.setRules(addRuleReq.getRules());
        return ruleEngineService.addRules(ruleMetadata);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/drool/getRuleById/{corpId}")
    public Optional<List<Rule>> getRuleById(@PathVariable String corpId) throws NoRuleFoundException {
        String path = corpId + ".drl";
        return ruleEngineService.getRuleByPath(path);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/drool/insertFact")
    public String insertFact(@RequestHeader Long corp, @RequestBody Map fact) {
        String groupName = corp+"";
        return ruleEngineService.insertFact(groupName, fact).toString();
    }


}
