package com.ad2pro.spectra.droolutil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolUtilApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroolUtilApplication.class, args);
    }

}
